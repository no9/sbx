# sbx

Investigation into sandboxing a rust rocket process on FreeBSD.
This could be useful if you want to run an application safely on a system where you don't want the management and processing overhead of containers.

## usage 

```
% git clone https://gitlab.com/no9/sbx.git
% cd sbx
% cargo run
% curl http://localhost:8080
{ "Hello", "world!" }
% curl http://localhost:8080/crash/now
<empty>
```

If you look at the console running the rust process you will see 

```
thread '<unnamed>' panicked at 'But can't open!: Os { code: 94, kind: Other, message: "Not permitted in capability mode" }', src/libcore/result.rs:1009:5
note: Run with `RUST_BACKTRACE=1` environment variable to display a backtrace.
```

# todo 

The [capsicum implementation](https://github.com/dlrobertson/capsicum-rs) works as expected but there is currently no integration with [casperd](https://www.unix.com/man-page/freebsd/8/casperd/) so calling out from the service needs additional work.