#![feature(proc_macro_hygiene, decl_macro)]
#[macro_use] extern crate rocket;
extern crate env_logger;
extern crate capsicum;

use capsicum::{enter, sandboxed};
use std::env;
use rocket::config::{Config, Environment};
use rocket::fairing::AdHoc;
use std::fs;

#[get("/")]
fn index() -> &'static str {
    "{ \"Hello\", \"world!\" }\n"
}

#[get("/<name>")]
fn crash(name: String) -> &'static str {
    fs::File::open("README.md").expect("But can't open!");
    "{ \"Hello\", \"world!\" }"
}

fn main() {

    env_logger::init();

    let key = "PORT";

    let port: String = match env::var(key) {
        Ok(val) => val,
        Err(_) => String::from("8080"),
    };
    let int_port: u16 = port.parse().unwrap();
    let config = Config::build(Environment::Production)
        .address("0.0.0.0")
        .port(int_port)
        .finalize();

    //Sandbox::new().sandbox_this_process();

    // use rocket_bsd::secureserve::{StaticFiles, Options};
//    use rocket_bsd::secureserve::{StaticFiles};
    rocket::custom(config.unwrap())
        .attach(AdHoc::on_launch("Launch Message", |_| {
            enter().expect("enter failed!");
            assert!(sandboxed(), "application is not sandboxed!");
        }))
        .mount("/", routes![index])
        .mount("/crash", routes![crash])
        .launch();
}